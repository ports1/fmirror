fmirror uses GNU autoconf so compiling in most Unix-like environments
should be easy.

The normal way to compile and install fmirror is:

 ./configure
 make
 make install

Which will install fmirror in /usr/local.

If this doesn't work, please send information about os and version
(you can get this information by running "uname -a") and the output of
the command that failed to <finnag@fast.no>

configure understands some options which can alter the installation
directories:

  --prefix=/blah       install everything in /blah/... instead of /usr/local
  --exec-prefix=/blah  put binaries in /blah/... instead of /usr/local/...
  --bindir=/blah/bin   put fmirror in /blah/bin instead of /usr/local/bin
  --mandir=/blah       put fmirror manpage in /blah/man1/fmirror.1
  --datadir=/blah      put fmirror README & example configs in /blah/fmirror/

The default directories are

prefix = /usr/local
exec_prefix = ${prefix}
bindir = ${exec_prefix}/bin
mandir = ${prefix}/man
datadir = ${prefix}/share

In addition, the Makefile supports setting DESTDIR, which will install
everything into ${DESTDIR}/<normal-path>, which can be useful for
automated package building (rpm somewhat confusingly calls this
concept a "build root"). Example:

./configure
make
make install DESTDIR=/tmp/inst-dir/fmirror
